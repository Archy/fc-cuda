#ifndef DATA_LOADER_H
#define DATA_LOADER_H

#include <string>
#include <memory>
#include <vector>

/*
 * MNIST test case
 */
struct test_case
{
	std::shared_ptr<unsigned char> data;
	unsigned char label;
};


/*
 * MNIST data loader
 */
class data_loader
{
	std::vector<test_case> dataset;
	int image_size;

public:
	data_loader();
	~data_loader();

	/*
	 * load mnist data to memory
	 */
	bool init(const std::string &img_path, const std::string &labels_path);

	std::vector<test_case> get_dataset()
	{
		return dataset;
	}

	int get_image_size()
	{
		return image_size;
	}

private:
	/*
	 * MNIST is stored in Big-Endian order
	 */
	static int reverse_int(const int i);

	bool load_images(const std::string &data_path);
	
	bool load_labels(const std::string &labels_path);
};

#endif /* DATA_LOADER_H */