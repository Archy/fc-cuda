#ifndef FC_NETWORK_H
#define FC_NETWORK_H

#include "DataLoader.h"
#include "kernel.cuh"

/*
 * Fully connected network with cuda support
 */
class fc_network
{
	std::shared_ptr<input_layer> input;
	std::vector<std::shared_ptr<fc_layer>> hidden_layers;

	data_loader dataset;

public:
	fc_network();
	~fc_network();

	/*
	 * Loads dataset, initializes cuda and creates network layers
	 */
	bool init(const std::string &img_path, const std::string &labels_path, 
				const std::vector<int> n_neurons);

	/*
	 * Trains network over 'iter' test cases
	 * 
	 * iter			- number of passes to perform
	 * n_threads	- threads per block
	 * n_blocks		- nubmer of blocks
	 */
	void do_forward_passes(const int iter, const int n_threads, const int n_blocks);

	/*
	 * Tests if forward pass done with cuda works correctly.
	 * Run only after init was called
	 * 
	 * n_threads	- threads per block
	 * n_blocks		- nubmer of blocks
	 */
	void test_cuda(const int n_threads, const int n_blocks);

private:
	/**
	 * Cast test case (char array) to float array
	 */
	std::vector<float> to_float(const test_case &test);

	/*
	 * CPU implementation of fully-connected layer
	 */
	std::vector<float> test_multiplication(const int n_neurons, const std::vector<float> &weights, 
									const std::vector<float> &input);
};

#endif /* FC_NETWORK_H */