#ifndef KERNEL_H
#define KERNEL_H

#include <memory>
#include <vector>

void free_cuda_resource(void *p);

/*
 * Input layer of network. Images data is copied here for every pass
 */
struct input_layer
{
	const int size;
	float *data;


	input_layer(const int size, float* data)
		: size(size), data(data)
	{
	}

	~input_layer()
	{
		free_cuda_resource((void*)data);
	}
};

/*
 * Hidden or output layer of network
 */
struct fc_layer
{
	const int n_neurons;
	const int intput_size;

	const float *input;
	float *weights;
	float *output;

	fc_layer(const int n_neurons, const int intput_size,
		const float *input, float *weights, float *output) :
		n_neurons(n_neurons), intput_size(intput_size), 
		input(input), weights(weights), output(output)
	{
	}

	~fc_layer()
	{
		free_cuda_resource((void*)weights);
		free_cuda_resource((void*)output);
	}
};

/*
 *
 */
int init_cuda();

/*
 * Create input layer
 * size		- nr of floats in layer
 */
std::shared_ptr<input_layer> create_input_layer(int size);

/*
 * Create hidden or output layer:
 * n_neurons	- number of neurons in layer
 * input_size	- nr of outputs in prev layer
 * prev_layer	- beggining of prev layer output memory
 */
std::shared_ptr<fc_layer> create_layer(const int n_neurons, const int input_size, const float *prev_layer);

/*
 * Copy memory region of <size> floats beggining at <src> on host to <dest> on device
 */
bool copy_to_device(const float* src, float* dest, const int size);

/*
 * Copy memory region of <size> floats beggining at <src> on device to <dest> on host
 */
bool copy_from_device(const float* src, float* dest, const int size);

/*
 * Performs forward pass for all layers in network
 * n_threads	- threads per block
 * n_blocks		- nubmer of blocks
 */
void do_forward_pass(const std::vector<std::shared_ptr<fc_layer>> &hidden_layers, 
					const int n_threads, const int n_blocks);


#endif /* KERNEL_H */