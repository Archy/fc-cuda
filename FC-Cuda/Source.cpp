#include "kernel.cuh"
#include "FCNetwork.h"

#include <cstdio>


int main()
{
	printf("Starting\n");

	fc_network network;
	if (network.init("train-images.idx3-ubyte", "train-labels.idx1-ubyte", { 100, 600, 20 }))
	{
		printf("Network init success\n");
	}
//	network.test_cuda(32, 1);
//	network.test_cuda(16, 2);
	
	const int passes = 100;
	network.do_forward_passes(passes, 32, 1);
	network.do_forward_passes(passes, 32, 4);
//	network.do_forward_passes(passes, 1, 1);

	
	int threads = 0;
	int blocks = 0;
	do
	{
		printf("Threads: ");
		scanf("%d", &threads);

		printf("Blocks: ");
		scanf("%d", &blocks);
		if (threads > 0 && blocks > 0)
		{
			network.do_forward_passes(passes, threads, blocks);
		}
	} while (threads > 0 && blocks > 0);

	printf("Finished\n");
	return 0;
}
