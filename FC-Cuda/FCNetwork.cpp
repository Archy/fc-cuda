#include "FCNetwork.h"

#include <algorithm>
#include <cassert>
#include <ctime>


fc_network::fc_network()
= default;


fc_network::~fc_network()
= default;

bool fc_network::init(const std::string& img_path, const std::string& labels_path, const std::vector<int> n_neurons)
{
	//load datase
	if (!dataset.init(img_path, labels_path))
	{
		printf("Failed to load dataset\n");
		return false;
	}

	//init cuda
	if (init_cuda())
		return false;

	//init input layer
	input = create_input_layer(dataset.get_image_size());

	//init hidden layers
	bool first = true;
	for(int n : n_neurons)
	{
		if (first)
		{
			hidden_layers.push_back(create_layer(n, input->size, input->data));
			first = false;
		}
		else
		{
			hidden_layers.push_back(create_layer(n, hidden_layers.back()->n_neurons, hidden_layers.back()->output));
		}
		if (hidden_layers.back() == nullptr)
			return false;	//creating layer failed
	}
	return true;
}

void fc_network::do_forward_passes(const int iter, const int n_threads, const int n_blocks)
{
	const auto begin = clock();

	for(int epoch = 0; epoch < iter; ++epoch)
	{
//		printf("\t\tEpoch %d\n", epoch);
		//copy test case to device
		auto data = to_float(dataset.get_dataset()[epoch]);
		if (!copy_to_device(data.data(), input->data, input->size))
		{
			printf("Copying test case data to input layer failed\n");
			return;
		}
			
		do_forward_pass(hidden_layers, n_threads, n_blocks);
	}

	const auto end = clock();
	double elapsed_ms = (double(end - begin) / (CLOCKS_PER_SEC / 1000.0));
	printf("Forward pass completed: %d[blocks] x %d[threads],\telapsed time: \t%.10f[ms]\n", n_blocks, n_threads, elapsed_ms);
}

void fc_network::test_cuda(const int n_threads, const int n_blocks)
{
	//do forward pass on cuda
	auto input = to_float(dataset.get_dataset()[0]);
	copy_to_device(input.data(), this->input->data, this->input->size);

	auto first_layer = hidden_layers.front();
	do_forward_pass({ first_layer }, n_threads, n_blocks);

	//copy weights and output from cuda
	const int weights_size = first_layer->n_neurons * first_layer->intput_size;
	std::vector<float> weights(weights_size);
	copy_from_device(first_layer->weights, weights.data(), weights_size);

	std::vector<float> output(first_layer->n_neurons);
	copy_from_device(first_layer->output, output.data(), first_layer->n_neurons);

	//count input*weights on cpu
	printf("CPU implementation\n");
	auto base = test_multiplication(first_layer->n_neurons, weights, input);
	printf("Testing outpust:\n");
	assert(output.size() == base.size());
	for(int i=0; i<output.size(); ++i)
	{
		printf("\t%.20f - %.20f\n", output[i], base[i]);
		assert((fabs(output[i] - base[i]) < 0.0001));
	}
	printf("GPU and CPU implementations are the same\n");
}

std::vector<float> fc_network::to_float(const test_case& test)
{
	std::vector<float> data;
	for(int i=0; i<dataset.get_image_size(); ++i)
	{
		data.push_back(static_cast<float>(test.data.get()[i]));
	}
	return data;
}

std::vector<float> fc_network::test_multiplication(const int n_neurons,  const std::vector<float>& weights, 
													const std::vector<float>& input)
 {
	const int input_size = input.size();
	std::vector<float> outputs;

	assert(n_neurons*input_size == weights.size());
	for(int n=0; n<n_neurons; ++n)
	{
		float sum = 0.0f;
		const int start_index = n * input_size;
//		const float* w = weights.data() + (n*input_size * sizeof(float));
		for(int i=0; i<input_size; ++i)
		{
			sum += input[i] * weights[start_index+i];
		}
		outputs.push_back(std::max(0.0f, sum));
	}
	return outputs;
}
