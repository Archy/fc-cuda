#include "kernel.cuh"
#include "device_launch_parameters.h"
#include "cuda_runtime.h"

#include <cstdio>
#include <random>
#include <iostream>

void print_error(cudaError_t cuda_error, const char* msg);

void free_cuda_resource(void *p)
{
	if (p!=nullptr)
		cudaFree(p);
}

/*
 * ReLu activation function
 */
__device__ float activation(const float in)
{
	return in > 0 ? in : 0.0f;
}

/*
 * Forward pass for single layer
 * n_neurons	- number of neurons in current layer
 * intput_size	- number of outputs in previous layer
 * 
 * input	- beggining of previous layer output region
 * weights	- beggining of current layer weights region
 * output	- beggining of current layer output region
 */
__global__ void forward_kernell(const int n_neurons, const int intput_size, 
								const float *input, const float *weights, float *output)
{
	const int id = (blockIdx.x * blockDim.x) + threadIdx.x;
	const int n_threads = blockDim.x * gridDim.x;

	int neuron_id = id;
	while(neuron_id < n_neurons)
	{
		const int start_index = neuron_id * intput_size;
		float sum = 0.0f;
		for(int i=0; i<intput_size; ++i)
		{
			sum += weights[start_index + i] * input[i];
		}
		output[neuron_id] = activation(sum);
		
		neuron_id += n_threads;
	}
}

void do_forward_pass(const std::vector<std::shared_ptr<fc_layer>> &hidden_layers, 
					const int n_threads, const int n_blocks)
{
	for(auto &layer : hidden_layers)
	{
		dim3 blocksPerGrid(n_blocks);
		dim3 threadsPerBlock(n_threads);
		forward_kernell <<<blocksPerGrid, threadsPerBlock>>>(layer->n_neurons, layer->intput_size,
			layer->input, layer->weights, layer->output);
	}
}


int init_cuda()
{
	int i;
	cudaError_t cuda_error;

	// query and print all cuda capable devices
	int dev_count;
	cuda_error = cudaGetDeviceCount(&dev_count);
	if (cuda_error != cudaSuccess) 
	{
		print_error(cuda_error, "getting Cuda capable devices");
		return cuda_error;
	}

	printf("Available devices:\n");
	for (i=0; i<dev_count; ++i)
	{
		cudaDeviceProp deviceProp = {};
		cuda_error = cudaGetDeviceProperties(&deviceProp, i);
		if (cuda_error != cudaSuccess)
		{
			print_error(cuda_error, "getting Cuda device properties");
			return cuda_error;
		}
		printf("[%d]Device %s:\n"
			"\tMax threads per block: %d\n"
			"\tMax threads dim x:%d\n"
			"\tDevice overlap: %s\n"
			"\tShared memory per block %d\n",
			i,
			deviceProp.name, 
			deviceProp.maxThreadsPerBlock,
			deviceProp.maxThreadsDim[0],
			deviceProp.deviceOverlap==1 ? "YES" : "NO",
			deviceProp.sharedMemPerBlock);
	}
	int choosen_dev = 0;
	if (dev_count > 1)
	{
		printf("Choose device: ");
		scanf("%d", &choosen_dev);
	}

	cuda_error = cudaSetDevice(choosen_dev);
	if (cuda_error != cudaSuccess) 
	{
		print_error(cuda_error, "initalizing Cuda");
	}
	return cuda_error;
}

std::shared_ptr<input_layer> create_input_layer(int size)
{
	float *input = nullptr;

	cudaError_t cuda_error = cudaMalloc((void**)&input, size * sizeof(float));
	if (cuda_error != cudaSuccess) 
	{
		print_error(cuda_error, "allocating input layer memory");
		return nullptr;
	}

	return std::make_shared<input_layer>(size, input);
}

std::shared_ptr<fc_layer> create_layer(const int n_neurons, const int input_size, const float *input)
{
	cudaError_t cuda_error;
	const int weights_size = n_neurons * input_size;

	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_real_distribution<float> dist(-1.0f, 1.0f);

	//create and initialize weights
	std::vector<float> initial_weights;
	for (int i=0; i<weights_size; ++i)
	{
		initial_weights.push_back(0.01f*dist(mt));
	}

	float *weights = nullptr;
	cuda_error = cudaMalloc((void**)&weights, weights_size * sizeof(float));
	if (cuda_error != cudaSuccess) 
	{
		print_error(cuda_error, "allocating weights memory");
		return nullptr;
	}

	cuda_error = cudaMemcpy(weights, initial_weights.data(), 
							weights_size * sizeof(float), cudaMemcpyHostToDevice);
	if (cuda_error != cudaSuccess) 
	{
		print_error(cuda_error, "coping inital weights");
		cudaFree(weights);
		return nullptr;
	}

	//alloc memory for output
	float *output = nullptr;
	cuda_error = cudaMalloc((void**)&output, n_neurons * sizeof(float));
	if (cuda_error != cudaSuccess) 
	{
		print_error(cuda_error, "allocating output memory");
		cudaFree(weights);
		return nullptr;
	}

	return std::make_shared<fc_layer>(n_neurons, input_size, input, weights, output);
}

bool copy_to_device(const float* src, float* dest, const int size)
{
	cudaError_t cuda_error = cudaMemcpy((void*)dest, src,
		size * sizeof(float), cudaMemcpyHostToDevice);
	if (cuda_error != cudaSuccess) 
	{
		print_error(cuda_error, "coping data to device");
		return false;
	}
	return true;
}

bool copy_from_device(const float* src, float* dest, const int size)
{
	cudaError_t cuda_error = cudaMemcpy((void*)dest, src,
		size * sizeof(float), cudaMemcpyDeviceToHost);
	if (cuda_error != cudaSuccess) 
	{
		print_error(cuda_error, "coping data from device");
		return false;
	}
	return true;
}

void print_error(cudaError_t cuda_error, const char* msg)
{
	printf("Cuda error when %s: %s\n%s\n",
		msg,
		cudaGetErrorName(cuda_error),
		cudaGetErrorString(cuda_error));
}