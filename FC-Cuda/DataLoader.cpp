#include "DataLoader.h"
#include <fstream>
#include <cstring>

data_loader::data_loader()
= default;


data_loader::~data_loader()
= default;


bool data_loader::init(const std::string &img_path, const std::string &labels_path)
{
	if (load_images(img_path))
	{
		return load_labels(labels_path);
	}
	return false;
}


int data_loader::reverse_int(const int i)
{
	unsigned char c1, c2, c3, c4;

	c1 = i & 255;
	c2 = (i >> 8) & 255;
	c3 = (i >> 16) & 255;
	c4 = (i >> 24) & 255;

	return ((int)c1 << 24) + ((int)c2 << 16) + ((int)c3 << 8) + c4;
}


bool data_loader::load_images(const std::string& data_path)
{
	std::ifstream file(data_path, std::ios::binary);
	if (file.is_open())
	{
		int magic_number = 0, n_rows = 0, n_cols = 0, number_of_images;

		//read magic number (lol) to check if it's valid mnist file
		file.read(reinterpret_cast<char *>(&magic_number), sizeof(magic_number));
		magic_number = reverse_int(magic_number);
		if (magic_number != 2051)
		{
			printf("Invalid MNIST image file!\n");
			return false;
		}

		//read #images, #rows, #cols
		file.read(reinterpret_cast<char*>(&number_of_images), sizeof(number_of_images));
		number_of_images = reverse_int(number_of_images);

		file.read(reinterpret_cast<char*>(&n_rows), sizeof(n_rows));
		n_rows = reverse_int(n_rows);

		file.read(reinterpret_cast<char*>(&n_cols), sizeof(n_cols));
		n_cols = reverse_int(n_cols);

		//count image size 
		image_size = n_rows * n_cols;

		//read images
		for (int i = 0; i < number_of_images; i++) {
			std::shared_ptr<unsigned char> data(new unsigned char[image_size], std::default_delete<unsigned char[]>());
			file.read(reinterpret_cast<char *>(data.get()), image_size);

			dataset.push_back({ data, 0 });
		}
		file.close();
		return true;
	}
	printf("Failed to open images file: %s\n", strerror(errno));
	return false;
}


bool data_loader::load_labels(const std::string& labels_path)
{
	std::ifstream file(labels_path, std::ios::binary);

	if (file.is_open())
	{
		int magic_number = 0, number_of_labels;
		file.read(reinterpret_cast<char *>(&magic_number), sizeof(magic_number));
		magic_number = reverse_int(magic_number);
		if (magic_number != 2049)
		{
			printf("Invalid MNIST labels file!\n");
			return false;
		}

		file.read(reinterpret_cast<char *>(&number_of_labels), sizeof(number_of_labels));
		number_of_labels = reverse_int(number_of_labels);
		if (number_of_labels != dataset.size())
		{
			printf("MNIST labels doesn't match images!\n");
			return false;
		}

		for (int i = 0; i < number_of_labels; i++) {
			unsigned char label;
			file.read(reinterpret_cast<char*>(&label), 1);
			dataset[i].label = label;
		}

		file.close();
		return true;
	}
	printf("Failed to open labels file: %s\n", strerror(errno));
	return false;
}
